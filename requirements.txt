alembic==1.4.3
aniso8601==8.1.0
astroid==2.4.2
certifi==2020.11.8
chardet==3.0.4
click==7.1.2
colorama==0.4.4
coverage==5.3
coveralls==2.2.0
docopt==0.6.2
Flask==1.1.2
Flask-Migrate==2.5.3
Flask-RESTful==0.3.8
Flask-SQLAlchemy==2.4.4
Flask-Testing==0.8.0
gunicorn==20.0.4
httpie==2.3.0
idna==2.10
isort==5.6.4
itsdangerous==1.1.0
Jinja2==2.11.2
lazy-object-proxy==1.4.3
Mako==1.1.3
MarkupSafe==1.1.1
mccabe==0.6.1
npm==0.1.1
optional-django==0.1.0
psycopg2-binary==2.8.6
Pygments==2.7.2
pylint==2.6.0
pylint-flask-sqlalchemy==0.2.0
PySocks==1.7.1
python-dateutil==2.8.1
python-editor==1.0.4
pytz==2020.4
PyYAML==5.3.1
requests==2.25.0
requests-toolbelt==0.9.1
six==1.15.0
SQLAlchemy==1.3.20
toml==0.10.2
urllib3==1.26.2
Werkzeug==1.0.1
wrapt==1.12.1
