.PHONY = help lint test coverage
.DEFAULT_GOAL = help

help:
	@echo "------------------------------HELP------------------------------------"
	@echo "make lint - command to lint the project with pylint"
	@echo "make test - command to test the project with unittests"
	@echo "make coverage - command to see the project coverage with unittests"
	@echo "----------------------------------------------------------------------"

lint:  ## Lint and static-check
	pylint departmentapp

test:  ## Run tests
	python3 -m unittest

coverage:  ## Run tests with coverage
	coverage erase
	coverage run --include=departmentapp/* -m unittest
	coverage report -m