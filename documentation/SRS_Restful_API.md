**Software Requirements Specification 
for EPAM External Project**

**_1. Overall description_**
 
 The project is aimed to develop RESTful web service for CRUD 
 operations and simple web application for managing departments and employees. 
 Web application should use web service to fetch the data from database.
  
**_2. Specific project description_**
 
Project type - Flask application<br>
Database - PostgreSQL<br>
Python technologies for working with database - SQLAlchemy


**_3. Functional requirements_**
 
 Requires Python packages:
- Flask==1.1.2
- Flask-Migrate==2.5.3
- Flask-RESTful==0.3.8
- Flask-SQLAlchemy==2.4.4
- psycopg2-binary==2.8.6
- requests==2.25.0
- pylint==2.6.0
- pylint-flask-sqlalchemy==0.2.0
- gunicorn==20.0.4

**_4. Database requirements_**

Two tables: "department" and "employee":
 - Departments should store their names 
 - Employees should store the following data: related department, employee name, employee surname, date of birth, salary

**_5. Web application requirements_**

The web application should allow:
- display a list of departments and the average salary (calculated automatically) for these departments
- display a list of employees in the departments with:<br> 
   -- an indication of the salary for each employee<br> 
   -- search field to search for employees born on a certain date or in the period between dates
- display a list of all employees from all departments
- change (add / edit / delete) the above data


The web application should use RESTful API web service for storing data and reading from database.

**_6. Departmnets_**<br><br>
**_6.1. List of departments_**

The mode is designed to view the list of departments.

**Main scenario:**
 - User selects menu item “Departments”;
 - Application displays list of Departments.
  
Below you can see HTML mockup for the page with list of departments.

![Department page](html_mockups/department_html.jpg)
 <br>
 The list displays the following columns:
 - Name – title of department, has link to the page, where user can see the list of department employees;
 - Average salary – calculated automatically average salary of all employees in certain department;
 - Amount of employees – amount of employees in certain department, has link to the page, where user can see the list 
 of department employees;
 - Edit, Delete buttons - button to edit department title, and button to delete department 
 (user cannot delete non-empty department, the button is deactivated)
 <br><br>
 User can add new department by pressing the button "Add department".


 <br><br>
 
 
 **_6.2. Add/edit department_**
  <br>
_**Main scenario:**
- User clicks the "Add"/"Edit" button in the departments list view mode;
- Application displays form to add/edit title;
- User enters department title and presses "Add" or "Update" button;
  <br><br>
 HTML page with the form to add/update department.
 ![Add or update department form](html_mockups/departments_form_html.jpg)
 <br>
 When adding/updating a department, the following details are entered:
 - department title
 
 <br><br>  
**_6.3. List of employees of certain department_**    

This mode is intended for viewing and editing the employees list of particular department.

**Main scenario:**
- User clicks hyperlink on particular department title (or amout of employees) at the departments viewing mode item
- Application displays list of employees, filtering by birthdates and "Add employee" button to add new employee. 

HTML mockup page with the list of employees for certain departments:

![Department employees page](html_mockups/department_empl_list_html.jpg)
 <br>
The list displays the following columns:
- Name Surname – client’s first name and last name;
- Salary - float number;
- Birthdate – employee’s date of birth;
- Department title – the department title, for which the employee belongs to;
- "Edit", "Delete" buttons - pressing "Edit" button leads to update form, after pressing "Delete" button 
the record about employee in database deletes.     
<br>
Filtering by date:
- In the employees list view mode, the user sets a date filter and presses the "Filter employees" button;
- The application will show the employees with a certain birthdate or in the period 
between dates.<br>

Restrictions:
- If "Birthdate from" and "Birthday to" are filled, then filtering employees by this period.
- If "Birthdate from" is blank, then filtering by "Birthdate to" only.
- If "Birthdate to" is blank, then filtering by "Birthdate from" only.
- If both data are blank, user will receive a message with request to fill the dates.
 
 <br><br>
 
**_6.4. List of all employees in all departments_**    

This mode is intended for viewing and editing full employees list of all departments.

**Main scenario:**
- User selects menu item "Employees";
- Application displays list of all employees, filtering by birth-dates and "Add employee" button to add new employee.
<br>
HTML mockup page with the list of all employees of all departments
![Department employees page](html_mockups/employees_html.jpg)
<br><br>

The list displays the following columns:
- Name Surname – client’s first name and last name;
- Salary - float number;
- Birthdate – employee’s date of birth;
- Department title – the department title, for which the employee belongs to;
- "Edit", "Delete" buttons - pressing "Edit" button leads to update form, after pressing "Delete" button 
the record about employee in database deletes.   
<br>

Filtering by date:
- In the employees list view mode, the user sets a date filter and presses the "Filter employees" button;
- The application will show the employees with a certain birthdate or in the period 
between dates.<br>

Restrictions:
- If "Birthdate from" and "Birthday to" are filled, then filtering employees by this period.
- If "Birthdate from" is blank, then filtering by "Birthdate to" only.
- If "Birthdate to" is blank, then filtering by "Birthdate from" only.
- If both data are blank, user will receive a message with request to fill the dates.
 
 <br><br>
 
 **_6.5. Add/Update employee form_**    

**Main scenario for Add:**
- User clicks the "Add" button in the employee's list view mode;
- Application displays form to enter employee data;
- User enters employee’s data and presses "Add employee" button;
- If any data is entered incorrectly, incorrect data messages are displayed;
- If entered data is valid, then record is adding to database;
- If error occurs, then error message is displaying;
- If new employee record is successfully added, then list of employees with added records is displaying.

**Main scenario for Edit:**
- User clicks the "Edit" button in the employees list view mode;
- Application displays form to update employee data with pre-filled data for thi employee from database;
- User make changes in employee’s data and presses "Update employee" button;
- If any data is entered incorrectly, special data messages are displayed;
- If entered data is valid, then record is adding to database;
- If error occurs, then error message is displaying;
- If employee record is successfully updated, then list of employees with added records is displaying.

 
HTML page with the form to add/update employee
![Add or update employee form](html_mockups/employees_form_html.jpg)

When adding/updating an employee, the following details are entered:
- Name – employee’s first name;
- Surname – employee’s last name;
- Salary – float number, it is not required field;
- Birthdate – employee’s birthdate.
- Department - department title for employee.

All data fields are required, except of Salary.