from setuptools import setup

setup(
    name='department-app',
    version='1.0.0',
    packages=['departmentapp', 'departmentapp.rest', 'departmentapp.utils', 'departmentapp.views',
              'departmentapp.models'],
    url='https://gitlab.com/lavtet/department-app.git',
    license='',
    author='Tetiana Kuzmuk',
    author_email='lavtet@gmail.com',
    description='Simple management system for departments and employees',
    install_requires=[
            'Flask==1.1.2',
            'Flask-Migrate==2.5.3',
            'Flask-RESTful==0.3.8',
            'Flask-SQLAlchemy==2.4.4',
            'psycopg2-binary==2.8.6',
            'requests==2.25.0',
            'gunicorn==20.0.4'
        ]
)
