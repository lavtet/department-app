Hello!  
Here you will find Python project for EPAM External Course. <br>
 
The project was aimed to develop RESTful web service for CRUD operations and simple web application 
for managing departments and employees. Web application should use web service to fetch the data from database.
 
Project type - Flask application<br>
Database - PostgreSQL<br>
Python technologies for working with database - SQLAlchemy
 
In folder [documentation](documentation) you can find detailed project description with a static mockup version of the application 
in HTML format with hardcoded data.   

 
_Project structure:_
 
department-app:<br>
|__ departmentapp (package that implements all the functionality) <br>
|__ |__ models (this package includes modules with Python classes describing DB models)<br>
|__ |__ rest (this package includes module with RESTful service implementation (CRUD operations))<br>
|__ |__ templates (this folder includes web app html templates)<br>
|__ |__ test (this package includes module with unit tests)<br>
|__ |__ utils (this package for request parsing interface, needed for packages views and rest)<br>
|__ |__ views (this package includes modules with Web controllers / views)<br>
|__  run.py (starting script)<br>
  
Project includes pylint plugin, which rated the code at 9.41/10.
 
Gitlab CI was established for this project. It has 2 stages: 
test (running unit-test module) and static analysis (running pylint).
  
Code coverage is available on coveralls.io (https://coveralls.io/jobs/72156698). 
It has only 60% for the main reason I didn't manage to write tests for project views, sorry for that. 
    
You can view my deployed project on [Heroku](https://lavtet-epam-dep-app.herokuapp.com/).
 
Sincerely,<br>
Tetiana Kuzmuk