"""Module for request parsing interface.
Needed for module views.routes and rest.restapi."""
from flask_restful import reqparse


department_post_args = reqparse.RequestParser()
department_post_args.add_argument("title", type=str,
                                  help="Title of the department is required", required=True)


department_put_args = reqparse.RequestParser()
department_put_args.add_argument("title", type=str,
                                 help="Title of the department is required", required=True)

department_filter_post_args = reqparse.RequestParser()
department_filter_post_args.add_argument("from_bdate", type=str,
                                       help="To filter for birthday you must "
                                            "fill at least one field!", required=True)
department_filter_post_args.add_argument("to_bdate", type=str,
                                         help="Upper limit of the filter by date")


employee_filter_post_args = reqparse.RequestParser()
employee_filter_post_args.add_argument("from_bdate", type=str,
                                       help="To filter for birthday you must "
                                            "fill at least one field!", required=True)
employee_filter_post_args.add_argument("to_bdate", type=str,
                                       help="Upper limit of the filter by date")


employee_post_args = reqparse.RequestParser()
employee_post_args.add_argument("first_name", type=str,
                                help="Name is required", required=True)
employee_post_args.add_argument("last_name", type=str,
                                help="Surname is required", required=True)
employee_post_args.add_argument("salary", type=float,
                                help="Salary", required=False)
employee_post_args.add_argument("birthdate", type=str,
                                help="Birthdate", required=False)
employee_post_args.add_argument("department_id", type=int,
                                help="Department is required", required=True)


employee_put_args = reqparse.RequestParser()
employee_put_args.add_argument("first_name", type=str,
                               help="Name is required", required=True)
employee_put_args.add_argument("last_name", type=str,
                               help="Surname is required", required=True)
employee_put_args.add_argument("salary", type=float,
                               help="Salary", required=False)
employee_put_args.add_argument("birthdate", type=str,
                               help="Birthdate", required=True)
employee_put_args.add_argument("department_id", type=int,
                               help="Department is required", required=True)
