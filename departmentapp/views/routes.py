"""Module contains all routes for our website (rendering html templates,
connecting them with flask-restful api resources)"""
import json
import requests
from flask import render_template, redirect, flash, request
from sqlalchemy.exc import OperationalError
from departmentapp import app, BASE
from departmentapp.utils import args as gen_args
from departmentapp.models.models import Department


@app.route("/")
def index():
    """:returns rendered main page """
    return render_template("index.html")


@app.route('/departments')
def departments():
    """Function renders the Department page with list of all departments (loaded from JSON)
     or handles an error page if db connection failed."""
    try:
        response = requests.get(url=BASE + "/api/departments").content
        dep_list = json.loads(response)
        if 'message' in dep_list:
            raise Exception(dep_list['message'])
        return render_template("departments.html", departments=dep_list)
    except Exception as err:
        return render_template("error_handler.html", message=str(err))


@app.route('/departments/<int:dep_id>', methods=['GET', 'POST'])
def departments_extended(dep_id):
    """Function renders the Department page with list of its employees (loaded from JSON)
     or handles an error page if db connection failed."""
    dep_response = Department.query.get(dep_id)
    if request.method == "POST":
        try:
            args = gen_args.department_filter_post_args.parse_args()
            from_bdate = args['from_bdate']
            to_bdate = args['to_bdate']


            # checking all possible options with dates below
            if from_bdate != '':
                if to_bdate != '':
                    response = requests.get(url=BASE + "/api/departments/" + str(dep_id) + "/"
                                            + from_bdate + "/" + to_bdate).content
                else:
                    response = requests.get(url=BASE + "/api/departments/" + str(dep_id)
                                            + "/" + str(from_bdate)).content
            elif to_bdate != '':
                response = requests.get(url=BASE + "/api/departments/" + str(dep_id) + "/"
                                        + str(to_bdate)).content
            else:
                flash('You cannot use filter without entering dates!')
                return redirect('/departments/' + str(dep_id))


            empl_list = json.loads(response)

            if 'message' in empl_list:
                if empl_list['message'] == 'Not found.':
                    flash("Sorry! There is no employees with such birth-dates. "
                          "You can make search through all employees on page 'Employees'!")
                    return render_template("department.html", department_empl=[],
                                           department_id=dep_id, dep_title=dep_response.title,
                                           flag=2)

                raise Exception(empl_list['message'])
            return render_template("department.html", department_empl=empl_list,
                                   department_id=dep_id, dep_title=dep_response.title, flag=1)
        except Exception as err:
            return render_template("error_handler.html", message=str(err))
    else:
        try:
            response = requests.get(
                url=BASE + "/api/departments/" + str(dep_id)
                    + "/employees", data={'id': dep_id}).content
            emp_list_dep = json.loads(response)
            if 'message' in emp_list_dep:
                raise Exception(emp_list_dep['message'])
            return render_template("department.html", department_empl=emp_list_dep,
                                   department_id=dep_id, dep_title=dep_response.title, flag=0)
        except Exception as err:
            return render_template("error_handler.html", message=str(err))


@app.route("/departments/add")
def add_department_form():
    """:return - renders page with a form for adding new department."""
    return render_template("department_form.html", department=None)


@app.route('/department/add', methods=["POST"])
def add_department():
    """Function receives data from the form (add_department_form),
    and calls the rest-api resource with method 'post' in order
    to add new departments' title to db """
    args = gen_args.department_post_args.parse_args()
    title = args['title']
    requests.post(url=BASE + "/api/departments", data={'title': title})
    flash('Department added successfully!')
    return redirect("/departments")


@app.route('/departments/<int:dep_id>/update')
def update_department_form(dep_id):
    """Function loads data about department from json api response
    and sends this data to the update form in html. It raises an error
    if it could not connect to the database"""
    try:
        response = requests.get(url=BASE + "/api/departments/" + str(dep_id),
                                data={'id': dep_id}).content
        dep = json.loads(response)
        if 'message' in dep:
            raise Exception(dep['message'])
        return render_template("department_form.html", department=dep)
    except Exception as err:
        return render_template("error_handler.html", message=str(err))


@app.route('/department/<int:dep_id>/update', methods=["POST"])
def update_department(dep_id):
    """Function receives data from the update form (update_department_form()),
        and calls the rest-api resource with method 'put' in order
        to update departments' title in db """
    args = gen_args.department_put_args.parse_args()
    title = args['title']

    requests.put(url=BASE + "/api/departments/" + str(dep_id), data={'id': dep_id, 'title': title})
    flash('Department updated successfully!')
    return redirect("/departments")


@app.route('/departments/<int:dep_id>/delete')
def delete_department(dep_id):
    """Function calls 'delete' rest-api resource in order to delete department from db.
    Redirects to the page with list of departments."""
    requests.delete(url=BASE + "/api/departments/" + str(dep_id), data={'id': dep_id})
    flash('Department deleted successfully!')
    return redirect("/departments")


@app.route('/employees', methods=['GET', 'POST'])
def employees():
    """Function works in 2 ways:
    1) if method 'POST' - filters the employees by birth-dates
    2) if 'GET' - gets the employee list from rest api resource and renders the html template."""
    if request.method == "POST":
        try:
            args = gen_args.employee_filter_post_args.parse_args()
            from_bdate = args['from_bdate']
            to_bdate = args['to_bdate']

            # checking all possible options with dates below
            if from_bdate != '':
                if to_bdate != '':
                    response = requests.get(url=BASE + "/api/employees/"
                                                + from_bdate + "/" + to_bdate).content
                else:
                    response = requests.get(url=BASE + "/api/employees/" + str(from_bdate)).content
            elif to_bdate != '':
                response = requests.get(url=BASE + "/api/employees/" + str(to_bdate)).content
            else:
                flash('You cannot use filter without entering dates!')
                return redirect('/employees')

            empl_list = json.loads(response)

            if 'message' in empl_list:
                raise Exception(empl_list['message'])

            if not empl_list:
                flash("Sorry! There is no employee with such birthdate.")
                return render_template("employees.html", employees=empl_list, flag=2)

            return render_template("employees.html", employees=empl_list, flag=1)
        except Exception as err:
            return render_template("error_handler.html", message=str(err))
    else:
        try:
            response = requests.get(url=BASE + "/api/employees").content
            empl_list = json.loads(response)
            if 'message' in empl_list:
                raise Exception(empl_list['message'])
            return render_template("employees.html", employees=empl_list)
        except Exception as err:
            return render_template("error_handler.html", message=str(err))


@app.route("/employees/add")
def add_employee_form():
    """:return - renders page with a form for adding new employee."""
    try:
        dep_list = Department.query.all()
        return render_template("create_employee.html", departments=dep_list)
    except OperationalError:
        return render_template("error_handler.html", message=app.config['DB_ERROR_MSG'])


@app.route('/employee/add', methods=["POST"])
def add_employee():
    """Function receives data from the form (add_employee_form),
        and calls the rest-api resource with method 'post' in order
        to add new employee to db """
    args = gen_args.employee_post_args.parse_args()
    first_name = args['first_name']
    last_name = args['last_name']
    salary = args['salary']
    birthdate = args['birthdate']
    department_id = args['department_id']
    requests.post(url=BASE + "/api/employees", data={'first_name': first_name,
                                                     'last_name': last_name,
                                                     'salary': salary, 'birthdate': birthdate,
                                                     'department_id': department_id})
    flash('Employee was added successfully!')
    return redirect("/employees")


@app.route('/employees/<int:empl_id>/update')
def update_employee_form(empl_id):
    """Function loads data about employee from json api response
        and sends this data to the update form in html. It raises an error
        if it could not connect to the database"""
    try:
        response = requests.get(url=BASE + "/api/employees/" + str(empl_id),
                                data={'id': empl_id}).content
        got_info_employee = json.loads(response)
        dep_list = Department.query.all()
        return render_template("edit_employee.html",
                               employee=got_info_employee,
                               departments=dep_list)
    except OperationalError:
        return render_template("error_handler.html", message=app.config['DB_ERROR_MSG'])


@app.route('/employee/<int:empl_id>/update', methods=["POST"])
def update_employee(empl_id):
    """Function receives data from the update form (update_employee_form()),
        and calls the rest-api resource with method 'put' in order
        to update employees' data in db """
    args = gen_args.employee_put_args.parse_args()

    first_name = args['first_name']
    last_name = args['last_name']
    salary = args['salary']
    birthdate = args['birthdate']
    department_id = args['department_id']

    requests.put(url=BASE + "/api/employees/" + str(empl_id), data={
        'id': empl_id, 'first_name': first_name, 'last_name': last_name,
        'salary': salary, 'birthdate': birthdate,
        'department_id': department_id})

    flash('Employee was updated successfully!')
    return redirect("/employees")


@app.route('/employees/<int:empl_id>/delete')
def delete_employee(empl_id):
    """Function calls 'delete' rest-api resource in order to delete employee from db.
        Redirects to the page with list of employees."""
    requests.delete(url=BASE + "/api/employees/" + str(empl_id), data={'id': empl_id})
    flash('Employee was deleted successfully!')
    return redirect("/employees")
