"""Module with SQLAlchemy models."""
from departmentapp import db


class Department(db.Model):
    """SQLAlchemy.Model for the Department table"""
    __tablename__ = 'department'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)

    def __init__(self, title, dep_id=None):
        self.title = title
        self.id = dep_id

    def __repr__(self):
        return f"<Department {self.id}>"


class Employee(db.Model):
    """SQLAlchemy.Model for the Employee table"""
    __tablename__ = 'employee'

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(25), nullable=False)
    last_name = db.Column(db.String(40), nullable=False)
    salary = db.Column(db.Float, nullable=False)
    birthdate = db.Column(db.Date, nullable=False)
    department_id = db.Column(db.Integer, db.ForeignKey("department.id"))

    def __init__(self, first_name, last_name, salary, birthdate, department_id):
        self.first_name = first_name
        self.last_name = last_name
        self.salary = salary
        self.birthdate = birthdate
        self.department_id = department_id

    def __repr__(self):
        return f"<Employee {self.id}>"

# pylint --load-plugins pylint_flask_sqlalchemy departmentapp/models
# ~ avoiding E1101 error from pylint
