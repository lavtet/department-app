""" __init__.py:
    1) makes 'departmentapp' directory a package
    2) contains config and main imports information for project.
    app - an instance of the Flask class for our web app
    api - an instance of Flask-restful class for CRUD resourceful routing
    db, migrate - instances of SQLAlchemy and Migrate classes, needed to work with database"""

import logging
from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


logging.getLogger().addHandler(logging.StreamHandler())
open('app.log', 'w').close()
logging.getLogger().addHandler(logging.FileHandler('app.log'))

app = Flask(__name__)
app.secret_key = 'SECRET_KEY'
api = Api(app)


# app.config['SERVER_NAME'] = "lavtet-epam-dep-app.herokuapp.com"
# BASE = 'https://' + app.config['SERVER_NAME']
BASE = 'http://127.0.0.1:5000'


DB_URI = "postgres://" \
         "jqpqosqitnypgl:39e57e6f7850cd5280e0655c686b87a60bd5559e9706d2a420104419c9b70000" \
         "@ec2-176-34-123-50" \
         ".eu-west-1.compute.amazonaws.com:5432/ddt06gnpver118"
app.config['SQLALCHEMY_DATABASE_URI'] = DB_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['DB_ERROR_MSG'] = 'Oops... Something wrong. Could not connect to db. ' \
                             'Please, check the connection and try again.'

db = SQLAlchemy(app)
migrate = Migrate(app, db)

from departmentapp.views import routes
from departmentapp.rest import restapi
