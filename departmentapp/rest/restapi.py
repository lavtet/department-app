"""API module that gets requests and sends response for CRUD operations
on Department and Employee model"""
from datetime import date
from flask import jsonify
from flask_restful import Resource, abort
from sqlalchemy.sql import func
from sqlalchemy.exc import OperationalError
from departmentapp import db, app, api
from departmentapp.models.models import Department, Employee
from departmentapp.utils import args as gen_args


# pylint: disable=no-self-use


class DepartmentListRes(Resource):
    """Resource that works with list of departments"""

    def get(self):
        """Get list of departments"""
        try:
            dep_list = db.session.query(
                Department.id, Department.title,
                func.avg(Employee.salary),
                func.count(Employee.id)
            ).outerjoin(Employee).group_by(Department.id).order_by(
                Department.title.asc()).all()

            if not dep_list:
                abort(404, message="Could not find any department")
            return jsonify([{'id': u[0],
                             'title': u[1],
                             'avg_sal': u[2],
                             'emp_cnt': u[3]} for u in dep_list])
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})

    def post(self):
        """Post new department into db"""
        try:
            args = gen_args.department_post_args.parse_args()

            new_dep = Department(title=args['title'])
            db.session.add(new_dep)
            db.session.commit()
            return jsonify({'id': new_dep.id,
                            'title': new_dep.title,
                            'avg_sal': 0,
                            'emp_cnt': 0})
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})


api.add_resource(DepartmentListRes, "/api/departments")


class DepartmentRes(Resource):
    """Resource that works with exact department - get title, update and delete"""

    def get(self, dep_id):
        """Get certain department record"""
        try:
            dep = Department.query.get_or_404(dep_id)
            return jsonify({'id': dep.id, 'title': dep.title})
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})

    def delete(self, dep_id):
        """Delete certain department record, only in case it has no employees"""
        try:
            dep = Department.query.get_or_404(dep_id)

            dep_emp_list = \
                db.session.query(Employee.id, Department.id).join(Department).filter(
                    Department.id == dep_id).all()

            if len(dep_emp_list) == 0:
                    db.session.delete(dep)
                    db.session.commit()
                    return jsonify({'message': 'Successfully deleted'})
            else:
                return jsonify({'message': 'You cannot delete department with employees in it.'})
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})

    def put(self, dep_id):
        """Update certain department record"""
        args = gen_args.department_put_args.parse_args()
        try:
            dep = Department.query.get(dep_id)
            if dep is None:
                return jsonify({"message": "Not found. There is no such department."})
            dep.title = args['title']
            db.session.commit()
            return jsonify({'message': 'Successfully updated'})
        except Exception as err:
            return jsonify({'message': app.config['DB_ERROR_MSG']})


api.add_resource(DepartmentRes, "/api/departments/<int:dep_id>")


class DepartmentListEmployeesRes(Resource):
    """Resource, which gets a list of certain department employees"""

    def get(self, dep_id):
        """Get certain department record and returns the list of this department employees"""
        try:
            dep_emp_list = db.session.query(Employee.id, Employee.first_name, Employee.last_name,
                                            Employee.salary, Employee.birthdate, Department.id,
                                            Department.title).join(Department).order_by(
                Employee.first_name.asc()).filter(Department.id == dep_id).all()

            if len(dep_emp_list) == 0:
                return jsonify({'message': 'Could not find employees for this department.'})

            return jsonify([{'id': u[0],
                             'first_name': u[1],
                             'last_name': u[2],
                             'salary': u[3],
                             'birthdate': u[4].strftime('%d-%m-%Y'),
                             'department_id': u[5],
                             'department_title': u[6]} for u in dep_emp_list])

        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})


api.add_resource(DepartmentListEmployeesRes, "/api/departments/<int:dep_id>/employees")


class DepartmentListEmployeesResExactDate(Resource):
    """Resource, which implements filter with exact birthdate
    through employees in certain department"""
    def get(self, dep_id, exact_date):
        """Get list of employees with certain birthdate"""
        try:
            emp_list = db.session.query(Employee.id, Employee.first_name, Employee.last_name,
                                        Employee.salary, Employee.birthdate, Department.id,
                                        Department.title).join(Department).order_by(
                Employee.first_name.asc()).filter(
                Department.id == dep_id).filter(Employee.birthdate == exact_date).all()

            if len(emp_list) == 0:
                return jsonify({'message': 'Not found.',
                                'department_id': dep_id})

            return jsonify([{'id': u[0],
                             'first_name': u[1],
                             'last_name': u[2],
                             'salary': u[3],
                             'birthdate': u[4].strftime('%d-%m-%Y'),
                             'department_id': u[5],
                             'department_title': u[6]} for u in emp_list])
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})


api.add_resource(DepartmentListEmployeesResExactDate,
                 "/api/departments/<int:dep_id>/<string:exact_date>")


class DepartmentListEmployeesResFromToDate(Resource):
    """Resource, which implements filter between two birth-dates
    through employees in certain department"""
    def get(self, dep_id, from_bdate, to_bdate):
        """Get list of employees with filter between birth-dates (from, to)"""
        try:
            emp_list = db.session.query(Employee.id, Employee.first_name,
                                        Employee.last_name, Employee.salary,
                                        Employee.birthdate, Department.id,
                                        Department.title).join(Department).order_by(
                Employee.first_name.asc()).filter(
                Department.id == dep_id).filter(
                Employee.birthdate.between(from_bdate, to_bdate)).all()

            if len(emp_list) == 0:
                return jsonify({'message': 'Not found.'})

            return jsonify([{'id': u[0],
                             'first_name': u[1],
                             'last_name': u[2],
                             'salary': u[3],
                             'birthdate': u[4].strftime('%d-%m-%Y'),
                             'department_id': u[5],
                             'department_title': u[6]}
                            for u in emp_list])
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})


api.add_resource(DepartmentListEmployeesResFromToDate,
                 "/api/departments/<int:dep_id>/<string:from_bdate>/<string:to_bdate>")


class EmployeesListRes(Resource):
    """Resource that works with list of employees"""
    def get(self):
        """Get list of employees"""
        try:
            emp_list = db.session.query(Employee.id, Employee.first_name,
                                        Employee.last_name, Employee.salary,
                                        Employee.birthdate, Department.id,
                                        Department.title).join(Department).order_by(
                Employee.first_name.asc()).all()
            return jsonify([{'id': u[0],
                             'first_name': u[1],
                             'last_name': u[2],
                             'salary': u[3],
                             'birthdate': u[4].strftime('%d-%m-%Y'),
                             'department_id': u[5],
                             'department_title': u[6]} for u in emp_list])
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})

    def post(self):
        """Post new employee into db"""
        try:
            args = gen_args.employee_post_args.parse_args()

            if args['salary'] is None:
                args['salary'] = 0

            if args['birthdate'] is None:
                args['birthdate'] = date.today()

            deps = Department.query.get(args['department_id'])

            if deps is None:
                return jsonify({"message": "You missed or there is no such department."})

            new_empl = Employee(first_name=args['first_name'],
                                last_name=args['last_name'],
                                salary=args['salary'],
                                birthdate=args['birthdate'],
                                department_id=args['department_id'])
            db.session.add(new_empl)
            db.session.commit()
            return jsonify({'id': new_empl.id,
                            'first_name': new_empl.first_name,
                            'last_name': new_empl.last_name,
                            'salary': new_empl.salary,
                            'birthdate': new_empl.birthdate.strftime('%d-%m-%Y'),
                            'dep_id': new_empl.department_id})
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})


api.add_resource(EmployeesListRes, "/api/employees")


class EmployeesListResExactDate(Resource):
    """Resource, which implements filter with exact birthdate
        through all employees in db"""
    def get(self, exact_date):
        """Get list of employees with certain birthdate"""
        try:
            emp_list = db.session.query(Employee.id, Employee.first_name, Employee.last_name,
                                        Employee.salary, Employee.birthdate, Department.id,
                                        Department.title).join(Department).order_by(
                Employee.first_name.asc()).filter(Employee.birthdate == exact_date).all()

            return jsonify([{'id': u[0],
                             'first_name': u[1],
                             'last_name': u[2],
                             'salary': u[3],
                             'birthdate': u[4].strftime('%d-%m-%Y'),
                             'department_id': u[5],
                             'department_title': u[6]} for u in emp_list])
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})


api.add_resource(EmployeesListResExactDate, "/api/employees/<string:exact_date>")


class EmployeesListResFromToDate(Resource):
    """Resource, which implements filter between two birthdates
        through all employees in db"""
    def get(self, from_bdate, to_bdate):
        """Get list of employees with filter between birth-dates (from, to)"""
        try:
            emp_list = db.session.query(Employee.id, Employee.first_name,
                                        Employee.last_name, Employee.salary,
                                        Employee.birthdate, Department.id,
                                        Department.title).join(Department).order_by(
                Employee.first_name.asc()).filter(Employee.birthdate.between(from_bdate, to_bdate))

            return jsonify([{'id': u[0],
                             'first_name': u[1],
                             'last_name': u[2],
                             'salary': u[3],
                             'birthdate': u[4].strftime('%d-%m-%Y'),
                             'department_id': u[5],
                             'department_title': u[6]}
                            for u in emp_list])
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})


api.add_resource(EmployeesListResFromToDate, "/api/employees/<string:from_bdate>/<string:to_bdate>")


class EmployeeRes(Resource):
    """Resource, that works with certain employee - get data, update, delete"""
    def get(self, emp_id):
        """Get certain employee record"""
        try:
            empl = Employee.query.get_or_404(emp_id)
            dep_title = Department.query.get(empl.department_id)

            return jsonify({
                'id': empl.id,
                'first_name': empl.first_name,
                'last_name': empl.last_name,
                'salary': empl.salary,
                'birthdate': empl.birthdate.strftime('%Y-%m-%d'),
                'department_id': empl.department_id,
                'department_title': dep_title.title})
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})

    def put(self, emp_id):
        """Update certain employee record"""
        args = gen_args.employee_put_args.parse_args()
        try:
            empl = Employee.query.get(emp_id)
            if empl is None:
                return jsonify({"message": "Not found"})

            empl.first_name = args['first_name']
            empl.last_name = args['last_name']

            if args['salary'] is None:
                args['salary'] = 0
            empl.salary = args['salary']

            empl.birthdate = args['birthdate']
            empl.department_id = args['department_id']
            db.session.commit()
            return {'message': 'Successfully updated'}
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})

    def delete(self, emp_id):
        """Delete certain employee record"""
        try:
            empl = Employee.query.get_or_404(emp_id)
            db.session.delete(empl)
            db.session.commit()
            return {'message': 'Successfully deleted'}
        except OperationalError:
            return jsonify({'message': app.config['DB_ERROR_MSG']})


api.add_resource(EmployeeRes, "/api/employees/<int:emp_id>")
