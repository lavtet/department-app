"""Module contains all unit tests for model classes and flask-restful api resources"""
import os
import sys
import datetime
import json
import unittest
from sqlalchemy.exc import OperationalError
import flask
from departmentapp.views import routes
from unittest.mock import patch
from departmentapp.models.models import Department, Employee
from departmentapp.rest.restapi import DepartmentListRes, DepartmentRes, \
    DepartmentListEmployeesRes, DepartmentListEmployeesResExactDate, \
    DepartmentListEmployeesResFromToDate, EmployeesListRes, \
    EmployeeRes, EmployeesListResExactDate, EmployeesListResFromToDate
from departmentapp import app, db

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
basedir = os.path.abspath(os.path.dirname(__file__))

# Below we setup initial data used by all tests implemented in class RestApiTests
department_1 = Department('Title 1', 1)
department_2 = Department('Title 2', 2)
department_3 = Department('Title 3', 3)
updated_title_department_2 = 'Updated Title 2'

employee_1 = Employee('Fname1', 'Lname1', 10, datetime.date(2020, 2, 10), department_1.id)
employee_1.id = 1
employee_2 = Employee('Fname2', 'Lname2', 20, datetime.date(2020, 2, 11), department_1.id)
employee_2.id = 2
employee_3 = Employee('Fname3', 'Lname3', 30, datetime.date(2020, 2, 12), department_2.id)
employee_3.id = 3
employee_4 = Employee('Fname4', 'Lname4', 40, datetime.date(2020, 2, 13), department_2.id)
employee_4.id = 4
employee_5 = Employee('Fname5', 'Lname5', 50, datetime.date(2020, 2, 14), department_2.id)
employee_5.id = 5
employee_6 = Employee('Fname6', 'Lname6', 60, datetime.date(2020, 2, 15), department_2.id)
employee_6.id = 6
employee_7 = Employee('Fname7', 'Lname7', 70, datetime.date(2020, 2, 16), department_2.id)
employee_7.id = 7


def save_departments_employees(*args):
    """Method that is used for saving the list of entities
    into DB given as list of parameters"""
    for entity in args:
        db.session.add(entity)

    db.session.commit()


def setup_departments_employees(dep_list, emp_list):
    """Method that is used for saving departments and employees
    into DB given as lists of parameters"""
    save_departments_employees(*dep_list)
    save_departments_employees(*emp_list)


class RestApiTests(unittest.TestCase):
    """Class which used to run tests for all model classes
    as well as all REST operations available in application"""

    @classmethod
    def setUpClass(cls):
        """setting up database uri, saving all departments and employees used in tests"""
        cls.db_uri = 'sqlite:///' + os.path.join(basedir, 'test.db')
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = cls.db_uri
        cls.app = app.test_client()
        db.create_all()
        cls.ctx = app.test_request_context()
        cls.ctx.push()
        # Let's setup 2 departments and 4 employees (2 per each department)
        setup_departments_employees([department_1, department_2],
                                    [employee_1, employee_2, employee_3, employee_4])

    @classmethod
    def tearDownClass(cls):
        """clearing all db modifications and test Flask context"""
        db.session.remove()
        db.drop_all()
        cls.ctx.pop()

    # below are mocks for two methods, which are used in post and put department operations
    @patch('departmentapp.utils.args.department_post_args.parse_args',
           return_value={'title': department_3.title})
    @patch('departmentapp.utils.args.department_put_args.parse_args',
           return_value={'title': updated_title_department_2})
    def test_departments(self, mock_update_department, mock_add_department):
        """Method that tests CRUD operations for Department including:
        - reading of all departments
        - reading of department by its ID
        - adding of new department
        - updating of existing department
        - deleting of existing department
        - updating, deleting, reading of department by non-existing ID"""
        dep_list_res = DepartmentListRes()
        response = dep_list_res.get()
        results = json.loads(response.response[0])

        # We should get 2 departments as per our initial setup
        self.assertTrue(len(results) == 2, 'There should be 2 departments')
        self.assert_department(results[0], (employee_1.salary + employee_2.salary) / 2, 2,
                               department_1.id,
                               department_1.title)
        self.assert_department(results[1], (employee_3.salary + employee_4.salary) / 2, 2,
                               department_2.id,
                               department_2.title)

        # Let's add one more department without employees
        dep_list_res.post()

        response = dep_list_res.get()
        results = json.loads(response.response[0])

        # Now we should get 3 departments after successful addition
        self.assertTrue(len(results) == 3, 'There should be 3 departments')
        self.assert_department(results[0], (employee_1.salary + employee_2.salary) / 2, 2,
                               department_1.id,
                               department_1.title)
        self.assert_department(results[1], (employee_3.salary + employee_4.salary) / 2, 2,
                               department_2.id,
                               department_2.title)
        self.assert_department(results[2], None, 0, department_3.id, department_3.title)

        dep_list_employees_res = DepartmentListEmployeesRes()

        # Let's check employee list of newly added department
        response = dep_list_employees_res.get(department_3.id)
        # There should be no employees
        self.assertTrue(
            'Could not find employees for this department.' in str(response.response[0]),
            'No employees in department expected')

        dep_res = DepartmentRes()
        dep_to_return = department_2

        # Let's check we can read department by its ID
        response = dep_res.get(dep_to_return.id)
        result = json.loads(response.response[0])

        # We should correctly read department as per our setup
        self.assertTrue(result['id'] == dep_to_return.id,
                        f'Department id is not as expected value "{dep_to_return.id}"')
        self.assertTrue(result['title'] == dep_to_return.title,
                        f'Department title is not as expected value "{dep_to_return.title}"')

        # Let's delete one of departments
        response = dep_res.delete(department_1.id)
        results = json.loads(response.response[0])

        # We should get error that the department with employees can't be deleted
        self.assertTrue('You cannot delete department with employees in it.'
                        in str(response.response[0]), 'Cannot delete department error is expected')

        # Let's delete department with no employees
        response = dep_res.delete(department_3.id)
        results = json.loads(response.response[0])

        # We should get message that the department was successfully deleted
        self.assertTrue('Successfully deleted'
                        in str(response.response[0]), 'Success message is expected.')

        response = dep_list_res.get()
        results = json.loads(response.response[0])
        # We should get 2 departments as a result of deletion
        self.assertTrue(len(results) == 2, 'There should be 2 departments')
        self.assert_department(results[0], (employee_1.salary + employee_2.salary) / 2, 2,
                               department_1.id,
                               department_1.title)
        self.assert_department(results[1], (employee_3.salary + employee_4.salary) / 2, 2,
                               department_2.id,
                               department_2.title)

        # Let's update one of departments
        dep_to_update = department_2
        dep_res.put(dep_to_update.id)
        response = dep_res.get(dep_to_update.id)
        result = json.loads(response.response[0])

        # We should see an effect of our update
        self.assertTrue(result['id'] == dep_to_update.id,
                        f'Department id is not as expected value "{dep_to_update.id}"')
        self.assertTrue(result['title'] == updated_title_department_2,
                        f'Department title is not as expected value "{dep_to_update.title}"')

        # Let's try to read non-existing department
        with self.assertRaises(Exception) as context:
            dep_res.get('non existing id')
            self.assertTrue('Not Found' in context.exception.name, 'Not Found error is expected')

        # Let's try to update non-existing department
        response = dep_res.put('non existing id')
        self.assertTrue('Not found. There is no such department.' in str(response.response[0]),
                        'Not Found error is expected')

        # Let's try to delete non-existing department
        with self.assertRaises(Exception) as context:
            dep_res.delete('non existing id')
            self.assertTrue('Not Found' in context.exception.name, 'Not Found error is expected')

    def test_dep_employees(self):
        """Method that tests CRUD operations for Departments Employees including:
            - reading all employees of certain department
            -
            """
        dep_emp_list_res = DepartmentListEmployeesRes()
        emp_list_dep_to_return = department_1
        response = dep_emp_list_res.get(emp_list_dep_to_return.id)
        results = json.loads(response.response[0])

        self.assertTrue(len(results) == 2, 'There should be 2 employees in department')
        self.assert_employee(results[0], employee_1.id, employee_1.first_name, employee_1.last_name,
                             employee_1.salary,
                             employee_1.birthdate, department_1.id, department_1.title)
        self.assert_employee(results[1], employee_2.id, employee_2.first_name, employee_2.last_name,
                             employee_2.salary,
                             employee_2.birthdate, department_1.id, department_1.title)

        # Let's try to filter employees in certain department by exact date
        emp_to_return = employee_2
        list_res_exact_date = DepartmentListEmployeesResExactDate()
        response = list_res_exact_date.get(emp_list_dep_to_return.id, emp_to_return.birthdate)
        result = json.loads(response.response[0])

        self.assertTrue(len(result) == 1, 'There should be only one employee by this exact date')
        self.assert_employee(result[0], emp_to_return.id, emp_to_return.first_name,
                             emp_to_return.last_name,
                             emp_to_return.salary,
                             emp_to_return.birthdate, emp_to_return.department_id)

        # Let's try to filter employee by exact non-existing date
        response = list_res_exact_date.get(emp_list_dep_to_return.id, datetime.date(2020, 1, 1))
        result = json.loads(response.response[0])

        # self.assertTrue(len(result) == 0, 'There should be no employees by this exact date')
        self.assertTrue('Not found.'
                        in str(response.response[0]), 'Not found. There should be no employees '
                                                      'by this exact date in certain department')

        # Let's try to filter employee by dates period
        list_res_dates_period = DepartmentListEmployeesResFromToDate()
        response = list_res_dates_period.get(emp_list_dep_to_return.id, employee_1.birthdate,
                                             employee_2.birthdate)
        results = json.loads(response.response[0])

        self.assertTrue(len(results) == 2,
                        'There should be 2 employees in certain department with birthdays '
                        'inside given period')
        self.assert_employee(results[0], employee_1.id, employee_1.first_name, employee_1.last_name,
                             employee_1.salary,
                             employee_1.birthdate, department_1.id, department_1.title)
        self.assert_employee(results[1], employee_2.id, employee_2.first_name, employee_2.last_name,
                             employee_2.salary,
                             employee_2.birthdate, department_1.id, department_1.title)

        # Let's try to filter employees in department by dates period outside existing dates
        response = list_res_dates_period.get(emp_list_dep_to_return.id, datetime.date(2019, 1, 1),
                                             datetime.date(2020, 1, 1))
        result = json.loads(response.response[0])

        self.assertTrue('Not found.' in str(response.response[0]),
                        'There should be no employees inside this period in this department')

    @patch('departmentapp.utils.args.employee_post_args.parse_args',
           return_value={'first_name': employee_5.first_name,
                         'last_name': employee_5.last_name,
                         'salary': employee_5.salary,
                         'birthdate': employee_5.birthdate,
                         'department_id': employee_5.department_id})
    @patch('departmentapp.utils.args.employee_put_args.parse_args',
           return_value={'first_name': employee_3.first_name,
                         'last_name': employee_3.last_name,
                         'salary': employee_3.salary,
                         'birthdate': employee_3.birthdate,
                         'department_id': department_2.id})
    def test_employees(self, mock_update_employee, mock_add_employee):
        """Method that tests CRUD operations for Employee including:
          - reading of all employees
          - reading of employee by its ID
          - adding of new employee
          - updating of existing employee
          - deleting of existing employee
          - updating, deleting, reading of employee by non-existing ID"""
        emp_list_res = EmployeesListRes()
        response = emp_list_res.get()
        results = json.loads(response.response[0])

        # We should get 4 employees for 2 departments
        self.assertTrue(len(results) == 4, 'There should be 4 employees')
        self.assert_employee(results[0], employee_1.id, employee_1.first_name, employee_1.last_name,
                             employee_1.salary,
                             employee_1.birthdate, department_1.id, department_1.title)
        self.assert_employee(results[1], employee_2.id, employee_2.first_name, employee_2.last_name,
                             employee_2.salary,
                             employee_2.birthdate, department_1.id, department_1.title)
        self.assert_employee(results[2], employee_3.id, employee_3.first_name, employee_3.last_name,
                             employee_3.salary,
                             employee_3.birthdate, department_2.id, department_2.title)
        self.assert_employee(results[3], employee_4.id, employee_4.first_name, employee_4.last_name,
                             employee_4.salary,
                             employee_4.birthdate, department_2.id, department_2.title)

        # Let's add one more employee
        emp_list_res.post()

        response = emp_list_res.get()
        results = json.loads(response.response[0])

        # Now we should get 5 employees after successful addition
        self.assertTrue(len(results) == 5, 'There should be 5 employees')
        self.assert_employee(results[0], employee_1.id, employee_1.first_name, employee_1.last_name,
                             employee_1.salary,
                             employee_1.birthdate, department_1.id, department_1.title)
        self.assert_employee(results[1], employee_2.id, employee_2.first_name, employee_2.last_name,
                             employee_2.salary,
                             employee_2.birthdate, department_1.id, department_1.title)
        self.assert_employee(results[2], employee_3.id, employee_3.first_name, employee_3.last_name,
                             employee_3.salary,
                             employee_3.birthdate, department_2.id, department_2.title)
        self.assert_employee(results[3], employee_4.id, employee_4.first_name, employee_4.last_name,
                             employee_4.salary,
                             employee_4.birthdate, department_2.id, department_2.title)
        self.assert_employee(results[4], employee_5.id, employee_5.first_name, employee_5.last_name,
                             employee_5.salary,
                             employee_5.birthdate, department_2.id, department_2.title)

        # Let's check we can read employee by its ID
        emp_res = EmployeeRes()
        emp_to_return = employee_4

        # We should correctly read employee
        response = emp_res.get(emp_to_return.id)
        result = json.loads(response.response[0])

        self.assert_employee(result, emp_to_return.id, emp_to_return.first_name,
                             emp_to_return.last_name,
                             emp_to_return.salary,
                             emp_to_return.birthdate, emp_to_return.department_id, None, '%Y-%m-%d')

        # Let's delete one of employees
        emp_res.delete(employee_4.id)
        response = emp_list_res.get()
        results = json.loads(response.response[0])

        # We should get 4 employees as a result of deletion
        self.assertTrue(len(results) == 4, 'There should be 4 employees')
        self.assert_employee(results[0], employee_1.id, employee_1.first_name, employee_1.last_name,
                             employee_1.salary,
                             employee_1.birthdate, department_1.id, department_1.title)
        self.assert_employee(results[1], employee_2.id, employee_2.first_name, employee_2.last_name,
                             employee_2.salary,
                             employee_2.birthdate, department_1.id, department_1.title)
        self.assert_employee(results[2], employee_3.id, employee_3.first_name, employee_3.last_name,
                             employee_3.salary,
                             employee_3.birthdate, department_2.id, department_2.title)
        self.assert_employee(results[3], employee_5.id, employee_5.first_name, employee_5.last_name,
                             employee_5.salary,
                             employee_5.birthdate, department_2.id, department_2.title)

        # Let's update one of employees
        emp_to_update = employee_3
        emp_res.put(emp_to_update.id)
        response = emp_res.get(emp_to_update.id)
        result = json.loads(response.response[0])

        # We should see an effect of our update
        self.assert_employee(result, emp_to_update.id, emp_to_update.first_name,
                             emp_to_update.last_name,
                             emp_to_update.salary,
                             emp_to_update.birthdate, department_2.id, None, '%Y-%m-%d')

        # Let's try to filter employee by exact date
        emp_to_return = employee_5
        list_res_exact_date = EmployeesListResExactDate()
        response = list_res_exact_date.get(emp_to_return.birthdate)
        result = json.loads(response.response[0])

        self.assertTrue(len(result) == 1, 'There should be only one employee by this exact date')
        self.assert_employee(result[0], emp_to_return.id, emp_to_return.first_name,
                             emp_to_return.last_name,
                             emp_to_return.salary,
                             emp_to_return.birthdate, emp_to_return.department_id)

        # Let's try to filter employee by exact non-existing date
        response = list_res_exact_date.get(datetime.date(2020, 1, 1))
        result = json.loads(response.response[0])

        self.assertTrue(len(result) == 0, 'There should be no employees by this exact date')

        # Let's try to filter employee by dates period
        list_res_dates_period = EmployeesListResFromToDate()
        response = list_res_dates_period.get(employee_3.birthdate, employee_5.birthdate)
        results = json.loads(response.response[0])

        self.assertTrue(len(results) == 2,
                        'There should be 2 employees with birthdays inside given period')
        self.assert_employee(results[0], employee_3.id, employee_3.first_name, employee_3.last_name,
                             employee_3.salary,
                             employee_3.birthdate, department_2.id, department_2.title)
        self.assert_employee(results[1], employee_5.id, employee_5.first_name, employee_5.last_name,
                             employee_5.salary,
                             employee_5.birthdate, department_2.id, department_2.title)

        # Let's try to filter employee by dates period outside existing dates
        response = list_res_dates_period.get(datetime.date(2019, 1, 1), datetime.date(2020, 1, 1))
        result = json.loads(response.response[0])

        self.assertTrue(len(result) == 0, 'There should be no employees inside this period')

        # Let's try to read non-existing employee
        with self.assertRaises(Exception) as context:
            emp_res.get('non existing id')
            self.assertTrue('Not Found' in context.exception.name, 'Not Found error is expected')

        # Let's try to update non-existing employee
        response = emp_res.put('non existing id')
        self.assertTrue('Not found' in str(response.response[0]), 'Not Found error is expected')

        # Let's try to delete non-existing employee
        with self.assertRaises(Exception) as context:
            emp_res.delete('non existing id')
            self.assertTrue('Not Found' in context.exception.name, 'Not Found error is expected')

    @patch('departmentapp.models.models.Department.query')
    @patch('departmentapp.db.session')
    @patch('departmentapp.utils.args.department_put_args.parse_args',
           return_value={'title': updated_title_department_2})
    def test_db_errors(self, dep_put_args, db_session, dep_query):
        dep_res = DepartmentRes()
        with patch('departmentapp.rest.restapi.Department.query.get_or_404',
                   side_effect=OperationalError(None, None, None)):
            response = dep_res.delete('non existing id')
            self.assert_db_error(response)

            response = dep_res.get('non existing id')
            self.assert_db_error(response)

        with patch('departmentapp.rest.restapi.Department.query.get',
                   side_effect=OperationalError(None, None, None)):
            response = dep_res.put('non existing id')
            self.assert_db_error(response)

        dep_list_res = DepartmentListRes()
        with patch('departmentapp.db.session.query',
                   side_effect=OperationalError(None, None, None)):
            response = dep_list_res.get()
            self.assert_db_error(response)

    def assert_db_error(self, response):
        self.assertTrue(
            'Oops... Something wrong. Could not connect to db. Please, check the connection and try again.' in str(
                response.response[0]),
            'We should have got DB connection error')

    def assert_department(self, department, avg_sal, emp_cnt, dep_id, title):
        """Method that executes asserts for Department instance"""
        if avg_sal is None:
            self.assertTrue(department['avg_sal'] is None,
                            'Employee average salary is not None as expected')
        else:
            self.assertTrue(department['avg_sal'] == avg_sal,
                            f'Employee average salary is not as expected value "{avg_sal}"')
        self.assertTrue(department['emp_cnt'] == emp_cnt,
                        f'Employee count id is not as expected value "{emp_cnt}"')
        self.assertTrue(department['id'] == dep_id,
                        f'Department id is not as expected value "{dep_id}"')
        self.assertTrue(department['title'] == title,
                        f'Title is not as expected value "{title}"')

    def assert_employee(self, employee, emp_id, first_name, last_name, salary, birthdate,
                        department_id, department_title=None, date_format='%d-%m-%Y'):
        """Method that executes asserts for Employee instance"""
        self.assertTrue(employee['id'] == emp_id,
                        f'Employee id is not as expected value "{emp_id}"')
        self.assertTrue(employee['first_name'] == first_name,
                        f'First name is not as expected value "{first_name}"')
        self.assertTrue(employee['last_name'] == last_name,
                        f'Last name is not as expected value "{last_name}"')
        self.assertTrue(employee['salary'] == salary,
                        f'Salary is not as expected value "{salary}"')
        self.assertTrue(employee['birthdate'] == birthdate.strftime(date_format),
                        f'Birthdate is not as expected value "{birthdate.strftime(date_format)}"')
        self.assertTrue(employee['department_id'] == department_id,
                        f'Department id is not as expected value "{department_id}"')
        if department_title is not None:
            self.assertTrue(employee['department_title'] == department_title,
                            f'Department title is not as expected value "{department_title}"')

    @patch.object(flask, 'render_template')
    def test_routes(self, mock):
        routes.render_template = mock

        routes.index()
        mock.assert_called_with('index.html')

    def test_repr_models(self):
        self.assertTrue(repr(department_1), f"<Department {department_1.id}>")
        self.assertTrue(repr(employee_2), f"<Employee {employee_2.id}>")


if __name__ == '__main__':
    unittest.main()
